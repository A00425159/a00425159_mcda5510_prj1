﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Reflection;
using System.Text.RegularExpressions;
using System.Web;

namespace CustomerPortal.Utility
{
    public class ValidatePostalCodeAttribute: ValidationAttribute
    {
        private readonly string _countryAttribute;
        public ValidatePostalCodeAttribute(string countryAttribute) : base("Invalid Postal Code")
        {
            _countryAttribute = countryAttribute;
        }

        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            var x = _countryAttribute;
            PropertyInfo propertyInfo = validationContext.ObjectType.GetProperty(_countryAttribute);

            var country = "";
            if (propertyInfo != null)
            {
                var countryObj = propertyInfo.GetValue(validationContext.ObjectInstance);
                if (countryObj != null)
                {
                    country = (string)countryObj;
                }
            }

           
            if (value != null)
            {
                var errorMessage = FormatErrorMessage(validationContext.DisplayName);
                var postalCode = value.ToString();
                switch (country)
                {
                    case "CA":
                        if (!IsValidCanadianPostalCode(postalCode))
                        {

                            return new ValidationResult(errorMessage);
                        }
                        break;
                    case "US":
                        if (!IsValidUSPostalCode(postalCode))
                        {

                            return new ValidationResult(errorMessage);
                        }
                        break;
                }
            }
            
            return ValidationResult.Success;
        }
        public static bool IsValidCanadianPostalCode(string number)
        {
            return Regex.Match(number,
                @"^([ABCEGHJKLMNPRSTVXY]\d[ABCEGHJKLMNPRSTVWXYZ])\ {0,1}(\d[ABCEGHJKLMNPRSTVWXYZ]\d)$").Success;
        }
        public static bool IsValidUSPostalCode(string number)
        {
            return Regex.Match(number, @"^\d{5}(?:[-\s]\d{4})?$").Success;
        }
    }
}