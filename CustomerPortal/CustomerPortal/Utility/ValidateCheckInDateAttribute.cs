﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Reflection;
using System.Web;

namespace CustomerPortal.Utility
{
    public class ValidateCheckInDateAttribute: ValidationAttribute
    {
       
        protected override ValidationResult IsValid(object value,
            ValidationContext validationContext)
        {

            DateTime str = (DateTime) value;

            if (str.Date < DateTime.Today)
            {
                var errorMessage = FormatErrorMessage(validationContext.DisplayName);
                return new ValidationResult(errorMessage);
            }

            return ValidationResult.Success;
        }

    }
    public class ValidateCheckOutDateAttribute : ValidationAttribute
    {
        private readonly string _checkInDateAttribute;
        public ValidateCheckOutDateAttribute(string attribute) : base("Invalid Phone Number")
        {
            _checkInDateAttribute = attribute;
        }

        protected override ValidationResult IsValid(object value,
            ValidationContext validationContext)
        {
            PropertyInfo propertyInfo = validationContext.ObjectType.GetProperty(_checkInDateAttribute);

            DateTime checkInDate;
            if (propertyInfo != null)
            {
                var countryObj = propertyInfo.GetValue(validationContext.ObjectInstance);
                if (countryObj != null)
                {
                    DateTime checkOutDate = (DateTime)value;
                    checkInDate = (DateTime) countryObj;
                    if (checkInDate.Date >= checkOutDate)
                    {
                        var errorMessage = FormatErrorMessage(validationContext.DisplayName);
                        return new ValidationResult(errorMessage);
                    }
                }

                

                
            }

            return ValidationResult.Success;
        }

    }
}