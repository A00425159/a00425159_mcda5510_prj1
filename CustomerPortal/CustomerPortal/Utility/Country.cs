﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Hosting;
using System.Web.Mvc;
using Newtonsoft.Json;
using Microsoft.VisualBasic.FileIO;

namespace CustomerPortal.Utility
{
    public class Country
    {
        public string CountryCode { get; set; }
        public string CountryName { get; set; }

        public static IEnumerable<SelectListItem> LoadCountryList()
        {
            string file = HostingEnvironment.MapPath(@"~/Content/CountryList.json");
            string s = HostingEnvironment.MapPath(@"~/App_Data/file.txt");
            //deserialize JSON from file  
            string json = System.IO.File.ReadAllText(file);


            dynamic dynamicObj = JsonConvert.DeserializeObject(json);
            List<Country> countries = new List<Country>();
            var selectList = new List<SelectListItem>();
            foreach (var item in dynamicObj.Country)
            {
                countries.Add(new Country { CountryCode = item.code, CountryName = item.name });
                selectList.Add(new SelectListItem
                {
                    Value = item.name,
                    Text = item.name
                });
            }

            return selectList;

        }

        private static List<Country> GetCountries()
        {
            string countryListCsv = HostingEnvironment.MapPath(@"~/Content/countryCodeList.csv");
            if (countryListCsv != null)
                using (TextFieldParser parser = new TextFieldParser(countryListCsv))
                {
                parser.Delimiters = new string[] { "," };
                    List<Country> countries = new List<Country>();
                    while (true)
                {
                    string[] parts = parser.ReadFields();
                    if (parts == null)
                    {
                        break;
                    }
                    
                    Country country = new Country();
                    country.CountryCode = parts[1];
                    country.CountryName = parts[0];
                    countries.Add(country);

                }

                    return countries;
                }
            return new List<Country>();
        }

        public static string GetCountry(string countryCode)
        {
            return GetCountryList().FirstOrDefault(x => x.CountryCode == countryCode).CountryName;
        }

        private static List<Province> GetProvinces()
        {
            string provinceListFile = HostingEnvironment.MapPath(@"~/Content/provinceList.csv");
            if (provinceListFile != null)
                using (TextFieldParser parser = new TextFieldParser(provinceListFile))
                {
                    parser.Delimiters = new string[] { "," };
                    List<Province> countries = new List<Province>();
                    while (true)
                    {
                        string[] parts = parser.ReadFields();
                        if (parts == null)
                        {
                            break;
                        }

                        Province province = new Province
                        {
                            CountryCode = parts[0],
                            ProvinceName = parts[2]
                        };
                        countries.Add(province);

                    }

                    return countries;
                }
            return new List<Province>();
        }

        public static List<Province> GetProvinces(string countryCode)
        {
            var xp = GetProvinceList().Where(ppp => ppp.CountryCode == countryCode).ToList();
            return GetProvinceList().Where(pp => pp.CountryCode == countryCode).ToList();
        }

        //Singleton Methods
            private static List<Country> _countries;
            private static List<Province> _provinces;

            public static List<Country> GetCountryList()
            {
                if (_countries == null)
                {
                    _countries = Country.GetCountries();
                }

                return _countries;
            }
            private static List<Province> GetProvinceList()
            {
                if (_provinces == null)
                {
                    _provinces = Country.GetProvinces();
                }

                return _provinces;
            }
        
    }

    public class Province
    {
        public string CountryCode { get; set; }
        public string ProvinceName { get; set; }
    }
}