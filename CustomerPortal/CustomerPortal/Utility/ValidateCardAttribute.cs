﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Reflection;
using System.Text.RegularExpressions;
using System.Web;

namespace CustomerPortal.Utility
{
    public class ValidateCardAttribute: ValidationAttribute
    {
        private readonly string _attribute;
        public ValidateCardAttribute(string attribute) : base("Invalid Phone Number")
        {
            _attribute = attribute;
        }

        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            PropertyInfo propertyInfo = validationContext.ObjectType.GetProperty(_attribute);

            var cardType = "";
            if (propertyInfo != null)
            {
                var obj = propertyInfo.GetValue(validationContext.ObjectInstance);
                if (obj != null)
                {
                    cardType = (string)obj;
                }
            }

            if (value != null)
            {
                var cardNo = value.ToString();
                if (!IsValidCard(cardNo,cardType))
                {
                    var errorMessage = FormatErrorMessage(validationContext.DisplayName);
                    return new ValidationResult(errorMessage);
                }
            }

            return ValidationResult.Success;
        }
        public static bool IsValidCard(string cardNumber, string cardType)
        {
            switch (cardType)
            {
                case "Visa":
                    //                                        return Regex.Match(cardNumber, @"^4\\d{15}").Success;
                    return Regex.Match(cardNumber, @"^4[0-9]{12}(?:[0-9]{3})?$").Success;
                case "Master Card":
                    //                    return Regex.Match(cardNumber, @"^5[1-5]{1}\\d{14}").Success;
                    return Regex.Match(cardNumber, @"^(?:5[1-5][0-9]{2}|222[1-9]|22[3-9][0-9]|2[3-6][0-9]{2}|27[01][0-9]|2720)[0-9]{12}$").Success;
                case "American Express":
                    //                    return Regex.Match(cardNumber, @"^3[47]{1}\\d{13}").Success;
                    return Regex.Match(cardNumber, @"^3[47][0-9]{13}$").Success;
            }

            return false;
        }
    }

    public class ValidateExpiryDateAttribute : ValidationAttribute
    {
        

        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {

            if (value != null)
            {
                var expiry = value.ToString();
                if (!IsValidExpiryDate(expiry))
                {
                    var errorMessage = FormatErrorMessage(validationContext.DisplayName);
                    return new ValidationResult(errorMessage);
                }
                   
                
            }

            return ValidationResult.Success;
        }
        public static bool IsValidExpiryDate(string number)
        {
            //            return Regex.Match(number, @"^\\d{1,3}[ (]{1}\\d{3}[ )]{1}\\d{3}\\s{0,1}\\d{4}$").Success;
            //return Regex.Match(number, @"^[\\+]{0,1}\\d{0,3}\\s{0,1}[(]{0,1}\\d{3}[)]{0,1}[ -]{0,1}\\d{3}[ -]{0,1}\\d{4}$").Success;
            return Regex.Match(number, @"^((0[1-9])|(1[0-2]))\/((2018)|(20[1-2][0-9]))$").Success;
        }
    }
}