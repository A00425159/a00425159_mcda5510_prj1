﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Text.RegularExpressions;
using System.Web;

namespace CustomerPortal.Utility
{
    public class ValidatePhoneAttribute: ValidationAttribute
    {
        private readonly string _countryAttribute;
        public ValidatePhoneAttribute(string countryAttribute) : base("Invalid Phone Number")
        {
            _countryAttribute = countryAttribute;
        }

        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            PropertyInfo propertyInfo = validationContext.ObjectType.GetProperty(_countryAttribute);

            var country = "";
            if (propertyInfo != null)
            {
                var countryObj =  propertyInfo.GetValue(validationContext.ObjectInstance);
                if (countryObj !=null)
                {
                    country = (string) countryObj;
                }
            }

            if ((country == "CA" || country == "US") && value != null)
            {
                var phoneNo = value.ToString();
                if (!IsValidCanadianPhoneNumber(phoneNo))
                {
                    var errorMessage = FormatErrorMessage(validationContext.DisplayName);
                    return new ValidationResult(errorMessage);
                }
        }
            return ValidationResult.Success;
        }
        public static bool IsValidCanadianPhoneNumber(string number)
        {
//            return Regex.Match(number, @"^\\d{1,3}[ (]{1}\\d{3}[ )]{1}\\d{3}\\s{0,1}\\d{4}$").Success;
            return Regex.Match(number, @"^[\+]{0,1}\d{0,3}\s{0,1}[(]{0,1}\d{3}[)]{0,1}[ -]{0,1}\d{3}[ -]{0,1}\d{4}$").Success;
            //return Regex.Match(number, @"\(?\d{3}\)?-? *\d{3}-? *-?\d{4}").Success;
        }
        
    }

}