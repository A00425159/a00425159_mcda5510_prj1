using CustomerPortal.Utility;

namespace CustomerPortal.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("CardInfo")]
    public partial class CardInfo
    {
        [StringLength(50)]
        public string CardType { get; set; }

        [StringLength(50)]
        [Display(Name = "Name on Card")]
        [Required(ErrorMessage = "Pleasee enter you Name")]
        public string CardName { get; set; }

        [Key]
        [StringLength(16)]
        [Required(ErrorMessage = "Pleasee enter you number")]
        [Display(Name = "Card No")]
//        [Range(0, 16, ErrorMessage = "Invalid Card Number")]
        [ValidateCard(nameof(CardType), ErrorMessage = "Invalid Card. Please Type Correctly")]
        public string CardNo { get; set; }

        [StringLength(50)]
        [Display(Name = "Expiry Date")]
        [Required(ErrorMessage = "Expiry Date is required")]
        [ValidateExpiryDate(ErrorMessage = "Invalid Expiry Date")]
        public string ExpiryDate { get; set; }

        [StringLength(4, ErrorMessage = "Invalid CVC")]
        [Display(Name = "CVC")]
        [Required(ErrorMessage = "CVC is required")]
        public string Cvc { get; set; }

        public static bool ValidateCreditCard(CardInfo cardInfo)
        {
            Random rnd = new Random();
            double crookedDice = rnd.Next(0, 100);
            if (crookedDice <= 15)
            {
                return false;
            }
            return true;
        }
    }
}
