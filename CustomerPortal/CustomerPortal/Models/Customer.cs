using System.Web.Mvc;
using CustomerPortal.Utility;
using CustomerPortal.ViewModels;

namespace CustomerPortal.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Customer
    {
        [Key]
        [ScaffoldColumn(false)]
        public int CustID { get; set; }

        [StringLength(50)]
        [Display(Name = "First Name")]
        public string FirstName { get; set; }

        [StringLength(50)]
        [Display(Name = "Last Name")]
        public string LastName { get; set; }

        [StringLength(50)]
        [Display(Name = "Street Number")]
        public string StreetNo { get; set; }

        [StringLength(50)]
        [Display(Name = "City")]
        public string City { get; set; }

        [StringLength(50)]
        [Required]
        [Display(Name = "Province")]
        public string Province { get; set; }

        [StringLength(50)]
        [Required]
        [Display(Name = "Country")]
        public string Country { get; set; }

        [StringLength(50)]
        [Display(Name = "Postal Code")]
        //[RegularExpression("^\\w\\d\\w\\s*\\d\\w\\d$", ErrorMessage = "Postal code is not valid")]
        [ValidatePostalCode(nameof(Country), ErrorMessage = "Postal Code not valid")]
        public string PostalCode { get; set; }

        //[StringLength(50)]
        //[RegularExpression("^[\\+]{0,1}\\d{0,3}\\s{0,1}[(]{0,1}\\d{3}[)]{0,1}[ -]{0,1}\\d{3}[ -]{0,1}\\d{4}$", ErrorMessage = "Phone Number is not valid")]
        [Display(Name = "Phone Number")]
        [ValidatePhone(nameof(Country), ErrorMessage = "Invalid Phone Number")]
        //[MaxWords(2, ErrorMessage = "skda sdjlkasjd asd")]
        public string PhoneNo { get; set; }

        [StringLength(50)]
        [Display(Name = "Email address")]
        [Required(ErrorMessage = "The email address is required")]
        [EmailAddress(ErrorMessage = "Invalid Email Address")]
        [RegularExpression("^[a-zA-Z0-9_\\.-]+@([a-zA-Z0-9-]+\\.)+[a-zA-Z]{2,6}$", ErrorMessage = "E-mail is not valid")]
        public string EmailAddress { get; set; }

        public IEnumerable<SelectListItem> Provinces { get; set; }
    }
}
