using CustomerPortal.Utility;
using System.Linq;
using CustomerPortal.ViewModels;

namespace CustomerPortal.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    public partial class Reservation
    {
        [Key]
        public int ReservationCode { get; set; }

        public int CustID { get; set; }

        [Column(TypeName = "date")]
        [Display(Name = "Check In Date")]
        [DataType(DataType.Date)]
        [ValidateCheckInDate(ErrorMessage = "You can not select previous date")]
        [Required]
        public DateTime? CheckInDate { get; set; }

        [Column(TypeName = "date")]
        [Display(Name = "Check Out Date")]
        [DataType(DataType.Date)]
        [Required]
        [ValidateCheckOutDate(nameof(CheckInDate),ErrorMessage = "Check out date must be greater than check in date")]
        public DateTime? CheckOutDate { get; set; }

        [Display(Name = "Number of Guests")]
        [Required]
        [Range(1, 400)]
        public int? NoGuests { get; set; }
        
        [Display(Name = "Number of Rooms")]
        [Required]
        [Range(1,400)]
        public int? NoRooms { get; set; }

        // GetAll reservations
        public static IList<Reservation> GetAll()
        {
            IList<Reservation> reservations = (from s in DbSingleton.GetDb().Reservations where s.CheckInDate >= DateTime.Today select s).ToList();

            return reservations;
        }

        public static IEnumerable<Reservation> Read()
        {
            return GetAll();
        }
    }
}
