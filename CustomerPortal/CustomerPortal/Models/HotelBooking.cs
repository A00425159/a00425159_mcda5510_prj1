namespace CustomerPortal.Models
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class HotelBooking : DbContext
    {
        public HotelBooking()
            : base("name=HotelBooking")
        {
        }

        public virtual DbSet<CardInfo> CardInfoes { get; set; }
        public virtual DbSet<Customer> Customers { get; set; }
        public virtual DbSet<Reservation> Reservations { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<CardInfo>()
                .Property(e => e.CardType)
                .IsFixedLength();

            modelBuilder.Entity<CardInfo>()
                .Property(e => e.CardName)
                .IsFixedLength();

            modelBuilder.Entity<CardInfo>()
                .Property(e => e.CardNo)
                .IsFixedLength();

            modelBuilder.Entity<CardInfo>()
                .Property(e => e.ExpiryDate)
                .IsFixedLength();

            modelBuilder.Entity<CardInfo>()
                .Property(e => e.Cvc)
                .IsFixedLength();

            modelBuilder.Entity<Customer>()
                .Property(e => e.FirstName)
                .IsFixedLength();

            modelBuilder.Entity<Customer>()
                .Property(e => e.LastName)
                .IsFixedLength();

            modelBuilder.Entity<Customer>()
                .Property(e => e.StreetNo)
                .IsFixedLength();

            modelBuilder.Entity<Customer>()
                .Property(e => e.City)
                .IsFixedLength();

            modelBuilder.Entity<Customer>()
                .Property(e => e.Province)
                .IsFixedLength();

            modelBuilder.Entity<Customer>()
                .Property(e => e.Country)
                .IsFixedLength();

            modelBuilder.Entity<Customer>()
                .Property(e => e.PostalCode)
                .IsFixedLength();

            modelBuilder.Entity<Customer>()
                .Property(e => e.PhoneNo)
                .IsFixedLength();

            modelBuilder.Entity<Customer>()
                .Property(e => e.EmailAddress)
                .IsFixedLength();
        }
    }
}
