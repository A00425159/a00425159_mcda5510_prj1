﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using CustomerPortal.ViewModels;

namespace CustomerPortal.Models
{
    public class CustomerService
    {
        public static IList<Customer> GetAll()
        {
            List<Customer> customers = (from s in DbSingleton.GetDb().Customers select s).ToList();

            return customers;
        }

        public static IEnumerable<Customer> Read()
        {
            return GetAll();
        }
    }
}