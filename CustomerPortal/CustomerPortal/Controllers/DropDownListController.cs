﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CustomerPortal.Utility;

namespace CustomerPortal.Controllers
{
    public class DropDownListController : Controller
    {
       

        public JsonResult Cascading_Get_Countries()
        {
            return Json(Country.GetCountryList(), JsonRequestBehavior.AllowGet);
        }

        public JsonResult Cascading_Get_Provinces(string Countries)
        {
            return Json(Country.GetProvinces(Countries), JsonRequestBehavior.AllowGet);
        }
    }
}