﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CustomerPortal.Models;
using CustomerPortal.Utility;
using CustomerPortal.ViewModels;

namespace CustomerPortal.Controllers
{
    public class HomeController : Controller
    {
        private string tempData = "temp";
        public ActionResult Index()
        {
            return View();
        }


        public ActionResult BookRoom()
        {
//            CustomerPortal.Models.Customer customer = new CustomerPortal.Models.Customer();
//            // Get all provinces
//            var provinces = GetAllProvinces();
//
//            var country = Country.GetCountryList();
//            var provincessss = Country.GetProvinces("CA");
//            customer.Provinces = GetSelectListItems(provinces);
//            BookRoomViewModel bookRoomViewModel = new BookRoomViewModel();
//            bookRoomViewModel.Customer = customer;
//            ViewBag.CountryList = Country.LoadCountryList();

            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult BookRoom(BookRoomViewModel bookRoomViewModel)
        {
            if (!ModelState.IsValid)
            {
                return BookRoom();
            }

            Customer customer = bookRoomViewModel.Customer;
            if (customer.Country != null)
            {
                customer.Country = Country.GetCountry(bookRoomViewModel.Customer.Country);
            }
            bookRoomViewModel.Customer = customer;
            TempData[tempData] = bookRoomViewModel;
            return RedirectToAction("Payment", "Home");
        }
        [HttpGet]
        public ActionResult ExistingCustomer(int custId)
        {

            Models.Customer customer = DbSingleton.GetDb().Customers.FirstOrDefault(x => x.CustID == custId);
            if (!Object.Equals(customer, null))
            {
                BookRoomViewModel bookRoomViewModel = new BookRoomViewModel();
                bookRoomViewModel.Customer = customer;
                return View(bookRoomViewModel);
            }
            return HttpNotFound();
        }

        [HttpGet]
        public ActionResult ExistingReservation(int reservationCode)
        {

            Models.Reservation reservation = DbSingleton.GetDb().Reservations.FirstOrDefault(x => x.ReservationCode == reservationCode);
            Models.Customer customer = DbSingleton.GetDb().Customers.FirstOrDefault(x => x.CustID == reservation.CustID);
            if (!Object.Equals(reservation, null))
            {
                BookRoomViewModel bookRoomViewModel = new BookRoomViewModel();
                bookRoomViewModel.Reservation = reservation;
                bookRoomViewModel.Customer = customer;
                return View(bookRoomViewModel);
            }
            return HttpNotFound();
        }

        [HttpPost]
        public ActionResult ExistingCustomer(BookRoomViewModel bookRoomViewModel)
        {
            if (!ModelState.IsValid)
            {
                return View();
            }
            else
            {
                // Saving state
                TempData[tempData] = bookRoomViewModel;
                return RedirectToAction("Payment");
;            }
        }

        [HttpPost]
        public ActionResult ExistingReservation(Reservation reservation)
        {
            return HttpNotFound();
        }


        public ActionResult CheckAvailableRooms()
        {
            return View();
        }
        public ActionResult CheckGuests()
        {
            return View();
        }

        public ActionResult CheckReservations()
        {
            return View();
        }

        public ActionResult Payment()
        {
            
            return View();
        }
        [HttpPost]
        public ActionResult Payment(CardInfo cardInfo)
        {
            if (!ModelState.IsValid)
            {
                return View();
            }

            BookRoomViewModel bookRoomViewModel = (BookRoomViewModel) TempData[tempData];
            BookRoomViewModel.SaveCustomerWithRoomDetails(bookRoomViewModel);
            return RedirectToAction("Confirm");
        }

        
        public ActionResult Confirm()
        {
            BookRoomViewModel bookRoomViewModel = (BookRoomViewModel)TempData[tempData];
            return View(bookRoomViewModel);
        }
       
        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
        private IEnumerable<SelectListItem> GetSelectListItems(IEnumerable<string> elements)
        {
            // Create an empty list to hold result of the operation
            var selectList = new List<SelectListItem>();
            foreach (var element in elements)
            {
                selectList.Add(new SelectListItem
                {
                    Value = element,
                    Text = element
                });
            }

            return selectList;
        }
        private IEnumerable<string> GetAllProvinces()
        {
            return new List<string>
            {
                "Nova Scotia",
                "New Brunswick",
                "Prince Edward Island",
                "Newfoundland",
                "Quebec",
                "Ontario",
                "Alberta",
                "Saskatchewan",
                "Manitoba",
                "British Columbia",
            };
        }
    }
}