﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CustomerPortal.Models;
using CustomerPortal.ViewModels;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;

namespace CustomerPortal.Controllers
{
    public partial class CustomerController : Controller
    {
//        [Demo]
//        public ActionResult Editing_Popup()
//        {
//            return View();
//        }
        public ActionResult Index()
        {
            return View();
        }
//
        public ActionResult EditingPopup_Read([DataSourceRequest] DataSourceRequest request)
        {
            return Json(CustomerService.Read().ToDataSourceResult(request));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult EditingPopup_Create([DataSourceRequest] DataSourceRequest request, Customer customer)
        {
            DbSingleton.GetDb().Customers.Add(customer);
            DbSingleton.GetDb().SaveChanges();
            return Json(CustomerService.Read().ToDataSourceResult(request));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult EditingPopup_Update([DataSourceRequest] DataSourceRequest request, Customer customer)
        {
            DbSingleton.GetDb().Customers.AddOrUpdate(customer);
            DbSingleton.GetDb().SaveChanges();
            return Json(CustomerService.Read().ToDataSourceResult(request));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult EditingPopup_Destroy([DataSourceRequest] DataSourceRequest request, Customer _customer)
        {
            Customer customer = DbSingleton.GetDb().Customers.FirstOrDefault(x => x.CustID == _customer.CustID);
            if (customer != null)
            {
                DbSingleton.GetDb().Customers.Remove(customer);
                DbSingleton.GetDb().SaveChanges();
            }
            
            return Json(CustomerService.Read().ToDataSourceResult(request));
        }



    }
}