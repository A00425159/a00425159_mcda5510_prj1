﻿using CustomerPortal.ViewModels;
using System.Web.Mvc;
using CustomerPortal.Models;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
 
namespace CustomerPortal.Controllers
{
    public partial class ReservationController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult EditingPopup_Read([DataSourceRequest] DataSourceRequest request)
        {
            return Json(Reservation.Read().ToDataSourceResult(request));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult EditingPopup_Create([DataSourceRequest] DataSourceRequest request, Reservation reservation)
        {
            return Json(Reservation.Read().ToDataSourceResult(request));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult EditingPopup_Update([DataSourceRequest] DataSourceRequest request, Reservation reservation)
        {

            if (ModelState.IsValid) { 
                Reservation res = DbSingleton.GetDb().Reservations.Find(reservation.ReservationCode);
                res.NoGuests = reservation.NoGuests;
                res.NoRooms = reservation.NoRooms;
                res.CheckInDate = reservation.CheckInDate;
                res.CheckOutDate = reservation.CheckOutDate;

                DbSingleton.GetDb().SaveChanges();
            }
            return Json(Reservation.Read().ToDataSourceResult(request, ModelState));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult EditingPopup_Destroy([DataSourceRequest] DataSourceRequest request, Reservation reservation)
        {
            //            Reservation.DeleteReservation(reservation);
            Reservation res = DbSingleton.GetDb().Reservations.Find(reservation.ReservationCode);
            DbSingleton.GetDb().Reservations.Remove(res);
            DbSingleton.GetDb().SaveChanges();

            return Json(Reservation.Read().ToDataSourceResult(request));
        }

        /*
        public static void DeleteReservation(Reservation reservation)
        {
            Reservation res = DbSingleton.GetDb().Reservations.Find(reservation.ReservationCode);
            DbSingleton.GetDb().Reservations.Remove(res);
            DbSingleton.GetDb().SaveChanges();
        }

        public static void UpdateReservation(Reservation reservation)
        {
            Reservation res = DbSingleton.GetDb().Reservations.Find(reservation.ReservationCode);
            res.NoGuests = reservation.NoGuests;
            res.NoRooms = reservation.NoRooms;
            res.CheckInDate = reservation.CheckInDate;
            res.CheckOutDate = reservation.CheckOutDate;

            DbSingleton.GetDb().SaveChanges();
        }
        */

    }
}