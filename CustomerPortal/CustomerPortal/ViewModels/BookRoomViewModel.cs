﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Hosting;
using System.Web.Mvc;  
using System.Web.Script.Serialization;
using CustomerPortal.Models;
using Newtonsoft.Json;
using System.Data.Entity.Migrations;


namespace CustomerPortal.ViewModels
{
    public class BookRoomViewModel
    {
        public Models.Customer Customer { get; set; }
        public Reservation Reservation { get; set; }

        private HotelBooking db = new HotelBooking();
        public static void SaveCustomerWithRoomDetails(BookRoomViewModel bookRoomViewModel)
        {
            try
            {

                // creates/updates the customer 
                DbSingleton.GetDb().Customers.AddOrUpdate(bookRoomViewModel.Customer);

                DbSingleton.GetDb().SaveChanges();

                Reservation reservation = bookRoomViewModel.Reservation;
                reservation.CustID = bookRoomViewModel.Customer.CustID;
                DbSingleton.GetDb().Reservations.Add(reservation);
                DbSingleton.GetDb().SaveChanges();
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
        }
    }

    public class DbSingleton
    {
        private static HotelBooking _db;

        public static HotelBooking GetDb()
        {
            if (_db == null)
            {
                _db = new HotelBooking();
            }

            return _db;
        }
    }
   
}